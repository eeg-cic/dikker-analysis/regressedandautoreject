clear;
clc;

addpath('functions')

infolder = uigetdir;

% get all subject csv file
subject_file = dir(infolder);
subject_file = subject_file([subject_file(:).isdir]==0);
subject_number = length(subject_file);

% create all subject output folder
subject_outfolder = {};
if ~exist('output_folder')
    mkdir('output_folder')
end
for nsub = 1:subject_number
    subject_outfolder = [subject_outfolder ['output_folder' filesep subject_file(nsub).name(1:end-4)]];
    mkdir(subject_outfolder{nsub});
end


for nsub = 1:subject_number
%     filename = 'C:\Users\coma_\Desktop\eeglabsandbox\Dikker_Analysis\RegressedAndAutoreject\data_example\Tan1.csv';
    filename = [subject_file(nsub).folder filesep subject_file(nsub).name];
    data = readtable(filename,'Delimiter',';');

    varNames = {'AF3','F7','F3','FC5','T7','P7','O1','O2','P8','T8','FC6','F4','F8','AF4','Rp','Musique','Tag'};
    data = table(data.AF3,data.F7,data.F3,data.FC5,data.T7,data.P7,data.O1,data.O2,...
        data.P8,data.T8,data.FC6,data.F4,data.F8,data.AF4,data.Rp_,data.Musique,data.Tag,'VariableNames',varNames);

    % do the regression
    signal = table2array(data(:,1:14));
    eye = [signal(:,2)-signal(:,13) signal(:,14)-signal(:,13)];
    [signal_coeff,signal_pca,~,~,~,~] = pca(signal, 'Economy',false);
    [~,eye_pca,~,~,~,~] = pca(eye, 'Economy', false);
    X = eye_pca;
    [~,~,Epca,~,~] = mvregress(X,signal_pca,'algorithm','cwls');
    regressed_signal = Epca/signal_coeff;
    data(:,1:14) = array2table(regressed_signal);

    % extract chief
    chief_marker = find(all(~isnan(data.Tag),2));
    chief_number = length(chief_marker)/2;
    chief_name = {};
    for i = 1:chief_number
        chief_name = [chief_name ['Chief' num2str(data.Tag(chief_marker(i*2)))]];
    end

    % extract song
    for i = 1:chief_number
        chief_section = data(chief_marker(i*2-1):chief_marker(i*2),:);
        change_logical = logical(diff(chief_section.Musique));
        event_marker = find(all(change_logical>0,2));
        
        if iscell(chief_section.Rp)
%             chief_section.Rp = cell2mat(chief_section.Rp);
            chief_section.Rp = str2double(chief_section.Rp);
        end
%         event_marker = find(all(~isnan(chief_section.Musique),2));
%         event_marker = event_marker(2:end);

        for j = 1:(length(event_marker)-1)
            section_start = event_marker(j);
            section_end = event_marker(j+1);

            switch chief_section.Musique(section_end)
                case 0
                    section_type = 'pause';
                case 1
                    section_type = 'music';
            end %end switch section_type

            section_data = chief_section(section_start:section_end,1:14);
            section_emotion = chief_section.Rp(section_start:section_end);
            
            section_outfolder = [subject_outfolder{nsub} filesep [chief_name{i} '_section' num2str(j) '_' section_type ]];
            
            %%%%%%%%%%%%%%%% epoch rejection %%%%%%%%%%%%%%%%%%
            % [number_epoch,cleaned_data] = rejection_automatic(section_data);
            signal = table2array(section_data)';
            [~,sample] = size(signal);
            number_epoch = floor(sample/128);
            signal = signal(:,1:(128*number_epoch));
            EEG = pop_importdata('dataformat','array','nbchan',14,'data','signal','setname','test','srate',128,'pnts',128,'xmin',0.008); 
            EEG = pop_jointprob(EEG,1,[1:14] ,6,2,1,0,0,[],0);
            reject_marker = find(EEG.reject.rejjp);
            for r=1:length(reject_marker)
                epoch_toreject = reject_marker(r);
                begin_rejection = ((epoch_toreject-1)*128) + 1;
                end_rejection = epoch_toreject*128;
                signal(:,begin_rejection:end_rejection) = 0;
            end
            cleaned_data = signal'; 
            
            %%%%%%%%%%%%%%%% Marker %%%%%%%%%%%%%%%%%%
            marker_towrite = cell(number_epoch+1,3);
            marker_towrite(1,1) = {'TL02'};
            
            for k = 1:number_epoch
                marker_towrite(k+1,1) = {((k-1)*128)+1};
                marker_towrite(k+1,2) = {((k-1)*128)+1};
                marker_towrite(k+1,3) = {'trig'};
            end
            
            %%%%%%%%%%%%%%% Emotion %%%%%%%%%%%%%%%%%%
            emotion_towrite = [];
            for k = 1:number_epoch
                start_emotion = (128*(k-1)) + 1;
                end_emotion = k*128;
                emotion_towrite = [emotion_towrite mode(section_emotion(start_emotion:end_emotion))];
            end
            
            mkdir(section_outfolder)
            writetable(cell2table(marker_towrite),[section_outfolder filesep [chief_name{i} '_section' num2str(j) '_' section_type 'regressed_autorejected'] '.eph.mrk'],'WriteVariableNames',false,'Delimiter','tab','FileType','text');
            saveeph([section_outfolder filesep [chief_name{i} '_section' num2str(j) '_' section_type 'regressed_autorejected'] '.eph'],cleaned_data,128)
            csvwrite([section_outfolder filesep 'emotion.csv'],emotion_towrite);
        end %end loop for event_marker

    end %end loop for each song extraction

end %loop for each subject




# RegressedAndAutoReject

This program is the first step for Dikker analysis. It can regrees eyeblink and perform auto rejection of epoch by using Makoto Pipeline algorithm (improbability test with 6SD)

&nbsp;  
## Requirements
1. Matlab 2016b
2. eeg_lab v14.1.1b

&nbsp;  
## Input: It require a structure of

-- folder_name  
|  
|---- subject1.csv  
|---- subject2.csv  
|---- and so on  

!!! subject csv file should be a file from Octopus sync whose signal is already filtered by cartool.

&nbsp;  
## Output: It will generate an output_folder which this structure

-- out_folder  
|  
|---- subject1_folder  
|&emsp;|------ chief1section1_song  
|&emsp;|------ chief1section2_pause  
|&emsp;|------ chief1section3_song  
|  
|---- subject1_folder  
&nbsp;&emsp;|------ chief1section1_song  
&nbsp;&emsp;|------ chief1section2_pause  
&nbsp;&emsp;|------ chief1section3_song  
&nbsp;&emsp;&emsp;|--------chief1section3_song.eph  
&nbsp;&emsp;&emsp;|--------chief1section3_song.eph.mrk  
&nbsp;&emsp;&emsp;|--------emotion.csv  
&nbsp;  
## Next step
For each .eph file in each folder you need to select and reject manually the epoch and create a .csv file which mark which epoch is rejected or which epoch is accepted  

For example: 1,0,1,1,1,0,0,1,1,0,1,0,1,1,0,0,0  
1 - accepted  
0 - rejected

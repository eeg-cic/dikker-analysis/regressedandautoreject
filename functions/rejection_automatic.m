function [ number_epoch, new_signal ] = rejection_automatic( signal )
    signal = table2array(signal)';
    [~,sample] = size(signal);
    number_epoch = floor(sample/125);
    signal = signal(:,1:(125*number_epoch));
    EEG = pop_importdata('dataformat','array','nbchan',14,'data','signal','setname','test','srate',128,'pnts',128,'xmin',0.008); 
    EEG = pop_jointprob(EEG,1,[1:14] ,6,2,1,0,0,[],0);
    reject_marker = find(EEG.reject.rejjp);
    for i=1:length(reject_marker)
        epoch_toreject = reject_marker(i);
        begin_rejection = ((epoch_toreject-1)*125) + 1;
        end_rejection = epoch_toreject*125;
        signal(:,begin_rejection:end_rejection) = 0;
    end
    new_signal = signal'; 
end

